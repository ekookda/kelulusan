-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 22, 2019 at 02:32 AM
-- Server version: 10.3.15-MariaDB-log
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekoalfar_kelulusan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_counter`
--

CREATE TABLE `tbl_counter` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `pengunjung` int(11) NOT NULL,
  `browser` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_counter`
--

INSERT INTO `tbl_counter` (`id`, `tanggal`, `ip_address`, `pengunjung`, `browser`) VALUES
(1, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(2, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(3, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(4, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(5, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(6, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(7, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(8, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(9, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(10, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(11, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(12, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(13, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(14, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(15, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(16, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(17, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(18, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(19, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(20, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(21, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(22, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(23, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(24, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(25, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(26, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(27, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(28, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(29, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(30, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(31, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(32, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(33, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(34, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(35, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(36, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(37, '2019-06-22', '::1', 1, 'Chrome/Opera'),
(38, '2019-06-22', '::1', 1, 'Chrome/Opera');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `link` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `is_main_menu` int(11) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `role_id`, `title`, `link`, `icon`, `is_main_menu`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fa fa-home', 0, 1),
(2, 1, 'Data Siswa', 'admin/siswa', 'fa fa-users', 0, 1),
(3, 1, 'Data laporan', 'admin/report', 'fa fa-file', 0, 1),
(9, 1, 'Jadwal Pengumuman', 'admin/timer', 'fa fa-calendar', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE `tbl_role` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `role`) VALUES
(1, 'Operator'),
(2, 'Student');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school`
--

CREATE TABLE `tbl_school` (
  `npsn` varchar(10) NOT NULL,
  `nama_sekolah` varchar(100) NOT NULL,
  `kabupaten` varchar(100) NOT NULL,
  `kepala_sekolah` varchar(50) NOT NULL,
  `NIP` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_school`
--

INSERT INTO `tbl_school` (`npsn`, `nama_sekolah`, `kabupaten`, `kepala_sekolah`, `NIP`) VALUES
('20106696', 'SMP Negeri 72', 'JAKARTA', 'Sandiaga Uno, S.Pd.', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_scores`
--

CREATE TABLE `tbl_scores` (
  `id` int(11) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `n_bindo` float(4,2) NOT NULL,
  `n_bing` decimal(4,2) NOT NULL,
  `n_mat` decimal(4,2) NOT NULL,
  `n_peminatan` decimal(4,2) NOT NULL,
  `n_total` decimal(6,2) NOT NULL,
  `n_rata` decimal(6,2) NOT NULL,
  `is_pass` varchar(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_scores`
--

INSERT INTO `tbl_scores` (`id`, `student_id`, `n_bindo`, `n_bing`, `n_mat`, `n_peminatan`, `n_total`, `n_rata`, `is_pass`) VALUES
(1, 'K010200630054', 70.00, '78.00', '80.00', '79.00', '307.00', '76.75', 'lulus'),
(2, 'K010200630063', 80.00, '80.00', '78.00', '76.00', '314.00', '78.50', 'lulus'),
(3, 'K010200630072', 67.00, '56.00', '56.00', '56.00', '235.00', '58.75', 'lulus'),
(4, 'K010200630089', 56.00, '45.00', '45.00', '45.00', '191.00', '47.75', 'tidak lulus'),
(5, 'K010200630098', 56.00, '65.00', '65.00', '65.00', '251.00', '62.75', 'tidak lulus'),
(6, 'K010200630099', 70.00, '80.00', '55.00', '65.00', '270.00', '67.50', 'lulus'),
(7, 'K010200630100', 80.00, '50.00', '77.00', '65.00', '272.00', '68.00', 'tidak lulus');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `telp` varchar(20) NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('L','P') NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`id`, `name`, `telp`, `birthday`, `gender`, `created_at`, `updated_at`) VALUES
('K010200630054', 'ADI KISWANTO', '081517406275', '1998-10-16', 'L', 1561165576, 1561165576),
('K010200630063', 'HERU PAMBUDI', '081517406276', '1998-10-17', 'L', 1561165576, 1561165576),
('K010200630072', 'ANANDA HARISTONANG', '081517406277', '1998-10-18', 'L', 1561165576, 1561165576),
('K010200630089', 'IDA MAULIDA', '081517406278', '1998-10-19', 'P', 1561165576, 1561165576),
('K010200630098', 'ANI YUDHOYONO', '081517406279', '1998-10-20', 'P', 1561165577, 1561165577),
('K010200630099', 'AGUS', '083898582004', '1995-07-10', 'L', 1561165577, 1561165577),
('K010200630100', 'IKA', '0895359066447', '1995-02-08', 'P', 1561165577, 1561165577);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(8) NOT NULL,
  `role_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `is_active` int(1) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `role_id`, `username`, `password`, `name`, `is_active`, `updated_at`) VALUES
(1, 1, 'admin', '$2y$10$9.dGUL1IDrZT92c9DmmUeOeXlrBkwe/9nzrIXTj9qrNHZ8jPcgNzu', 'Administrator', 1, 1561163277),
(102, 2, 'K010200630099', '$2y$10$CXIiPVc9CMQ1y1HuLlGTQ.12hdHmKvUF4Efze/00LH2mZ5Lb3ict.', 'AGUS', 1, 1561165577),
(103, 2, 'K010200630100', '$2y$10$sz4ZS2.t3A42gWHp3A9mFOTcayUD1CQMTKG8SY77T9IxkK7CWuViq', 'IKA', 1, 1561165577),
(101, 2, 'K010200630098', '$2y$10$/6nAi5VZ.ImdyijH53IJM.KllRKfQ9dnBfO559g5U/SsgXeAAFSr6', 'ANI YUDHOYONO', 1, 1561165577),
(100, 2, 'K010200630089', '$2y$10$V.MPwM/Os33K.2HTJ8mWRuEmwv2vpSyTQ7CRz/lLPZB6OfP0.eTnW', 'IDA MAULIDA', 1, 1561165577),
(97, 2, 'K010200630054', '$2y$10$gKb8PKhAioVXpwga7eOX8.BOgzE8s4Dd3XYc3H6s0o84EhliMagO6', 'ADI KISWANTO', 1, 1561165576),
(98, 2, 'K010200630063', '$2y$10$7ekk/Y/9PrrrJTM2dzSGXuGnBWYNbxF8vnRFdOZu6tI0TA5cJkpaC', 'HERU PAMBUDI', 1, 1561165576),
(99, 2, 'K010200630072', '$2y$10$nOYOsJt5F.KEKrud3/f0K.gMmLcSKfA941ocXHBdlv1H.y.iZLjoS', 'ANANDA HARISTONANG', 1, 1561165576);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_school`
--
ALTER TABLE `tbl_school`
  ADD PRIMARY KEY (`npsn`);

--
-- Indexes for table `tbl_scores`
--
ALTER TABLE `tbl_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_id` (`student_id`) USING BTREE;

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_scores`
--
ALTER TABLE `tbl_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
