<?php
echo doctype('html5');
echo "<html>";
echo "<head>";

// meta
echo meta('description', 'Aplikasi Tabungan Siswa');
echo meta('Content-type', 'text/html; charset=utf-8', 'equiv');
echo meta('X-UA-Compatible', 'IE=edge');
echo meta('keywords', 'tabungan, siswa');
// <!-- Tell the browser to be responsive to screen width -->
echo meta('viewport', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no');

echo "<title>Form Login</title>";

// link_tag
echo link_tag('assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/font-awesome-4.3.0/css/font-awesome.css', 'stylesheet', 'text/css');
echo link_tag('assets/ionicons.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/parsley.css', 'stylesheet', 'text/css');
?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    input.parsley-success,
    select.parsley-success,
    textarea.parsley-success {
        color: #468847;
        background-color: #DFF0D8;
        border: 1px solid #D6E9C6;
    }

    input.parsley-error,
    select.parsley-error,
    textarea.parsley-error {
        color: #B94A48;
        background-color: #F2DEDE;
        border: 1px solid #EED3D7;
    }

    .parsley-errors-list {
        margin: 2px 0 3px;
        padding: 0;
        list-style-type: none;
        font-size: 0.9em;
        line-height: 0.9em;
        opacity: 0;
        color: #B94A48;

        transition: all .3s ease-in;
        -o-transition: all .3s ease-in;
        -moz-transition: all .3s ease-in;
        -webkit-transition: all .3s ease-in;
    }

    .parsley-errors-list.filled {
        opacity: 1;
    }
</style>
<?php
echo "</head>";
echo "<body class='hold-transition login-page'>";

// Form Login
echo "<div class='login-box'>";
echo "<div class='login-logo'>";
echo anchor('#', 'SMP Negeri 72 Jakarta');
echo "</div>";
// echo "<p style='color: #000;font-weight: 400;text-align: center;'><i>'Sistem Informasi Pengumuman Kelulusan'</i></p>";

// login-logo
echo "<div class='login-box-body'>";
echo "<p class='login-box-msg'>Silahkan login untuk melihat hasil kelulusan Tahun Pelajaran 2018/2019</p>";
// echo heading('<i class="fa fa-user-circle-o"></i> Login', '1', array('class' => 'login-box-msg'));

echo $this->session->flashdata('message');

// form login
echo form_open('auth', array('id' => 'formLogin', 'data-parsley-validate' => ''));

echo "<div class='form-group has-feedback'>";
echo form_input('username', set_value('username'), array('id' => 'username', 'class' => 'form-control', 'placeholder' => 'Username atau Nomor Ujian', 'required' => 'required'));
echo "<span class='glyphicon glyphicon-user form-control-feedback'></span>";
echo "<span class='text-danger'>" . form_error('username') . "</span>";
echo "</div>";

echo "<div class='form-group has-feedback'>";
echo form_password('password', '', array('id' => 'password', 'class' => 'form-control', 'placeholder' => 'Tanggal Lahir', 'required' => 'required'));
echo "<span class='glyphicon glyphicon-lock form-control-feedback'></span>";
echo "<span class='text-danger'>" . form_error('password') . "</span>";
echo "<input type='checkbox' class='form-checkbox' id='show_password'> Tampilkan Password";
echo "</div>";

echo "<div class='row'>";
echo "<div class='col-xs-8'>";
//echo "<p>Mau tau pengumuman kelululusan SMA ? " . anchor('Pengumuman/', '<span class="label label-danger">klik disini</span>', 'style="text-decoration:none"') . "</p>";
echo "</div>"; // .col-xs-8

// .col
echo "<div class='col-offset-xs-8 col-xs-4'>";
echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-block btn-flat', 'name' => 'btn-login', 'id' => 'btn-login', 'content' => '<i class="fa fa-sign-in"></i> Sign In'));
echo "</div>"; // .col-xs-4
echo "</div>"; // .row
echo form_close();

echo "<br>";
echo "<p>No Ujian: ditulis lengkap sesuai nomor di Kartu Ujian, contoh (K01XXYYYYZZZZM)<br />
        Password menggunakan tanggal lahir (DDMMYY), misal 13 Mei 1999 maka 13051999<br />
        Jika ada kesalahan data, No Ujian dan Password</a><br>
        Cek kembali kartu ujian anda.
        <br /><i>Tampilan terbaik gunakan Google Chrome.</i></p>";

echo "</div>"; // .login-box-body
echo heading('Copyright <i class="fa fa-copyright"></i> 2016 - ' . Date("Y"), 5, array('class' => 'text-center'));
echo "</div>"; // .login-box
?>

<script type="text/javascript" src="<?= base_url(); ?>assets/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/jquery-validation/dist/parsley.js"></script>
<script type="text/javascript">
    $(function() {
        $('#formLogin').parsley().on('field:validated', function() {
            var ok = $('.parsley-error').length === 0;
        }).on('form:submit', function() {
            return true;
        });

        // show password
        $('#show_password').on('click', function() {
            if ($(this).is(':checked')) {
                $('#password').attr('type', 'text');
            } else {
                $('#password').attr('type', 'password');
            }
        });
    });
</script>
</body>

</html>