</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.19
    </div>
    <strong>Copyright &copy; 2017-<?= date('Y'); ?> <a href="http://ekoalfarisi.blogspot.com">Eko Alfarisi</a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url('assets/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js'); ?>"></script>
<!-- AdminLTE App.js -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
<!-- DataTables JS -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.js'); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js'); ?>" type="text/javascript"></script>