<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />

<style type="text/css">
    #user_profile {
        width: 128px;
        height: 128px;
        display: block;
        margin-left: auto;
        margin-right: auto
    }
</style>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <!-- Box -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-file-o" aria-hidden="true"></i> Laporan</h3>
                </div>
                <!-- /.box-header -->
                <!-- Profile Image -->
                <div class="box box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo form_open('', ['method' => 'get', 'class' => 'form-inline']); ?>
                            <div class="form-group">
                                <label for="filter" class=""><i class="fa fa-filter" aria-hidden="true"></i> Filter Berdasarkan :</label>
                                <select name="filter" id="filter" class="form-control">
                                    <?php $url = $_GET['filter']; ?>
                                    <option value="0" <?php if ($url == 0 || empty($url)) echo "selected"; ?>>-- Tampilkan Semua --</option>
                                    <option value="1" <?php if ($url == 1) echo "selected"; ?>>Lulus</option>
                                    <option value="2" <?php if ($url == 2) echo "selected"; ?>>Tidak Lulus</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Tampilkan</button>
                            <a href="<?php echo site_url('admin/laporan'); ?>" class="btn btn-link">Reset Filter</a>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <hr>
                    <h4><?= $keterangan; ?></h4>
                    <a href="<?php echo $url_cetak; ?>" class="btn btn-info btn-flat" target="_blank"><i class="fa fa-print" aria-hidden="true"></i> Cetak PDF</a>

                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Nomor Peserta</th>
                                    <th class="text-center">Nama Peserta</th>
                                    <th class="text-center">Nilai B.Indo</th>
                                    <th class="text-center">Nilai B.Ingg</th>
                                    <th class="text-center">Nilai MTK</th>
                                    <th class="text-center">Nilai IPA</th>
                                    <th class="text-center">Nilai Total</th>
                                    <th class="text-center">Nilai Rerata</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $n = 1;
                                if (!empty($data_siswa)) :
                                    foreach ($data_siswa as $row) :
                                        if ($row['is_pass'] != 'lulus') $status = "Tidak Lulus";
                                        else $status = "Lulus";
                                        ?>
                                        <tr>
                                            <td class="text-center"><?= $n++; ?></td>
                                            <td class="text-center"><?= $row['student_id']; ?></td>
                                            <td class="text-left"><?= $row['name']; ?></td>
                                            <td class="text-center"><?= $row['n_bindo']; ?></td>
                                            <td class="text-center"><?= $row['n_bing']; ?></td>
                                            <td class="text-center"><?= $row['n_mat']; ?></td>
                                            <td class="text-center"><?= $row['n_peminatan']; ?></td>
                                            <td class="text-center"><?= $row['n_total']; ?></td>
                                            <td class="text-center"><?= $row['n_rata']; ?></td>
                                            <td class="text-center"><?= $status; ?></td>
                                        </tr>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.box -->

        </div>
    </div>

</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->


<?php $this->load->view('template/foot'); ?>