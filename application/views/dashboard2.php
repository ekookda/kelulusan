<?php
$this->load->view('template/head');
?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />

<style type="text/css">
    /* body {
        background: #101123;
        font-family: sans-serif;
    }

    .main {
        margin: auto 10%;
        padding-top: 8%;
        text-align: center;
        color: #696969;
    }

    h4 {
        margin-bottom: 40px;
        font-size: 30px;
        color: #fff;
    }

    #clock>div {
        border-radius: 5px;
        background: #ffa500;
        padding: 20px 20px 10px 20px;
        color: #fff;
        display: inline-block;
    }

    #clock>div>p {
        font-size: 16px;
    }

    #days,
    #hours,
    #minutes,
    #seconds {
        padding: 20px;
        font-size: 20px;
        background: #fb8e00;
        display: inline-block;
        width: 40px;
    }

    span.turn {
        animation: turn 0.7s ease;
    }

    @keyframes turn {
        0% {
            transform: rotateX(0deg)
        }

        100% {
            transform: rotateX(360deg)
        }
    } */
</style>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Main content -->
<section class="content">
    <!-- Info boxes -->
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Jumlah Siswa Keseluruhan</span>
                    <span class="info-box-number"><?= $this->db->get('tbl_student')->num_rows(); ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <?php
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        ?>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-map-marker" aria-hidden="true"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">IP Address</span>
                    <span class="info-box-number"><?= $ipaddress; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-eye" aria-hidden="true"></i> Lihat Nilai UN</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <p>Berdasarkan hasil Ujian Nasional Berbasis Komputer (UNBK) pada tanggal 222 - 25 April 2019 bahwa :</p>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <th width="220px">Nomor Ujian</th>
                                    <td><?= $student[0]['student_id']; ?></td>
                                </tr>
                                <tr>
                                    <th>Nama Siswa</th>
                                    <td><?= $student[0]['name']; ?></td>
                                </tr>
                                <tr>
                                    <th>Kelas/Jenjang</th>
                                    <td>IX / SMP</td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Bahasa Indonesia</th>
                                    <td><?= $student[0]['n_bindo']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Matematika</th>
                                    <td><?= $student[0]['n_mat']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Bahasa Inggris</th>
                                    <td><?= $student[0]['n_bing']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Peminatan/IPA</th>
                                    <td><?= $student[0]['n_peminatan']; ?></td>
                                </tr>
                                <tr>
                                    <th class="text-primary">JUMLAH</th>
                                    <td class="text-primary">
                                        <?php
                                        $total = ($student[0]['n_peminatan'] + $student[0]['n_bing'] + $student[0]['n_mat'] + $student[0]['n_bindo']);
                                        $rata = ($student[0]['n_peminatan'] + $student[0]['n_bing'] + $student[0]['n_mat'] + $student[0]['n_bindo']) / 4;
                                        echo "<strong>" . $total . "</strong>";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Rata-rata</th>
                                    <td><?= $rata; ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 col-md-offset-3">
                            <?php
                            $is_paid = $student[0]['is_paid'];
                            $is_pass = $student[0]['is_pass'];

                            if ($is_paid == 1) :
                                ?>
                                <!-- Alert Bootstrap -->
                                <div class="alert alert-success">
                                    <p>Dinyatakan <strong>LULUS</strong>, <strong>SELAMAT!</strong></p>
                                </div>
                            <?php else : ?>
                                <div class="alert alert-danger">
                                    <p>Anda harus <strong>melunasi</strong> administrasi!</p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->

</section><!-- /.content -->

<!-- <div class="main">
    <h4>Countdown Timer</h4>
    <div id="clock">
        <div><span id="days"></span>
            <p>Hari</p>
        </div>
        <div><span id="hours"></span>
            <p>Jam</p>
        </div>
        <div><span id="minutes"></span>
            <p>Menit</p>
        </div>
        <div><span id="seconds"></span>
            <p>Detik</p>
        </div>
    </div>
</div> -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Informasi Penting</h4>
            </div>
            <div class="modal-body">
                <p align='center'><b>PENGUMUMAN KELULUSAN<br>
                        SMK NEGERI & SWASTA DKI JAKARTA<br>
                        TAHUN PELAJARAN 2018/2019
                        <br>
                        <br>
                        HIMBAUAN PASCA PENGUMUMAN<br>
                        <br></b>
                    <p align='left'>Kepada seluruh peserta didik Kelas XII dan XIII SMK Tahun Pelajaran 2018/2019 untuk <b>tidak melakukan</b> hal-hal berikut :</p>
                    <ol>
                        <li>
                            <p align='left'>Corat-coret baju dan objek lainnya;
                        </li>
                        <li>
                            <p align='left'>Konvoi kendaraan bermotor;
                        </li>
                        <li>
                            <p align='left'>Kumpul atau nge-geng di tempat-tempat tertentu;
                        </li>
                        <li>
                            <p align='left'>Tawuran;
                        </li>
                        <li>
                            <p align='left'>Pemalakan/bullying;
                        </li>
                        <li>
                            <p align='left'>Hal-hal lain yang melanggar ketertiban umum dan melanggar hukum.
                        </li>
                    </ol>
                    <p align='left'>Bagi peserta didik yang melanggar instruksi di atas, maka kepadanya akan diberikan sangsi yang tegas oleh pihak sekolah.</p>
                    <br>
            </div>
            <div class="modal-footer">
                <a style="color: black;" class="close" href="login.php">
                    <h4>Saya sudah membaca dan memahami instruksi di atas, dan selanjutnya saya siap melaksanakan!</h4>
                </a>
            </div>
        </div>

    </div>
</div>

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard2.js') ?>" type="text/javascript"></script>

<script>
    $('#myModal').modal('show');
    // function animation(span) {
    //     span.className = "turn";
    //     setTimeout(function() {
    //         span.className = ""
    //     }, 700);
    // }

    // function Countdown() {

    //     setInterval(function() {

    //         var hari = document.getElementById("days");
    //         var jam = document.getElementById("hours");
    //         var menit = document.getElementById("minutes");
    //         var detik = document.getElementById("seconds");

    //         var deadline = new Date("May 13, 2019 10:00:00");
    //         var waktu = new Date();
    //         var distance = deadline - waktu;

    //         var days = Math.floor((distance / (1000 * 60 * 60 * 24)));
    //         var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //         var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    //         var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    //         if (days < 10) {
    //             days = '0' + days;
    //         }
    //         if (hours < 10) {
    //             hours = '0' + hours;
    //         }
    //         if (minutes < 10) {
    //             minutes = '0' + minutes;
    //         }
    //         if (seconds < 10) {
    //             seconds = '0' + seconds;
    //         }

    //         hari.innerHTML = days;
    //         jam.innerHTML = hours;
    //         menit.innerHTML = minutes;
    //         detik.innerHTML = seconds;
    //         //animation
    //         animation(detik);
    //         if (seconds == 0) animation(menit);
    //         if (minutes == 0) animation(jam);
    //         if (hours == 0) animation(hari);

    //     }, 1000);
    // }

    // Countdown();
</script>

<?php $this->load->view('template/foot'); ?>