<?php $this->load->view('template/head'); ?>

<!--tambahkan custom css disini-->
<!-- iCheck -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css') ?>" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css') ?>" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker -->
<link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css" />

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><i class="fa fa-bullhorn"></i> Pengumuman Hasil Kelulusan</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h1 class="box-title"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;<?= $sekolah[0]['nama_sekolah']; ?>&nbsp;<?= ucwords(strtolower($sekolah[0]['kabupaten'])); ?></h1>
                    <p class="pull-right">Tanggal: <?= date('d-m-Y'); ?></p>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <p>Berdasarkan Hasil Rapat Dewan Guru <?= $sekolah[0]['nama_sekolah']; ?>&nbsp;<?= ucwords(strtolower($sekolah[0]['kabupaten'])); ?> menerangkan bahwa peserta didik :</p>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <table class="table table-hover table-condensed">
                                <tr>
                                    <th>Nama </th>
                                    <td><?= ucwords(strtolower($users[0]['name'])); ?></td>
                                </tr>
                                <tr>
                                    <th width="220px">Nomor Ujian</th>
                                    <td><?= $users[0]['student_id']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Bahasa Indonesia</th>
                                    <td><?= $users[0]['n_bindo']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Matematika</th>
                                    <td><?= $users[0]['n_mat']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Bahasa Inggris</th>
                                    <td><?= $users[0]['n_bing']; ?></td>
                                </tr>
                                <tr>
                                    <th class="pull-right">Nilai Peminatan/IPA</th>
                                    <td><?= $users[0]['n_peminatan']; ?></td>
                                </tr>
                                <tr>
                                    <th class="text-primary">JUMLAH</th>
                                    <td class="text-primary">
                                        <?php
                                        $total = ($users[0]['n_peminatan'] + $users[0]['n_bing'] + $users[0]['n_mat'] + $users[0]['n_bindo']);
                                        $nem = ($users[0]['n_peminatan'] / 10) + ($users[0]['n_bing'] / 10) + ($users[0]['n_mat'] / 10) + ($users[0]['n_bindo'] / 10);
                                        $rata = ($users[0]['n_peminatan'] + $users[0]['n_bing'] + $users[0]['n_mat'] + $users[0]['n_bindo']) / 4;
                                        echo "<strong>" . $total . "</strong>";

                                        if ($users[0]['is_paid'] == 'lunas' or $users[0]['is_paid'] == 1) {
                                            $is_paid = '<span class="label label-success">Lunas</span>';
                                        } else {
                                            $is_paid = '<span class="label label-danger">Belum Melunasi</span>';
                                        }

                                        if ($users[0]['is_pass'] == "lulus" or $users[0]['is_pass'] == '1') {
                                            $is_pass = '<span class="label label-success">Lulus</span>';
                                        } else {
                                            $is_pass = '<span class="label label-danger">Tidak Lulus</span>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-info">NEM</th>
                                    <td class="text-info">
                                        <?= round($nem, 2); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Rata-rata nilai</th>
                                    <td><?= $rata . " (rata-rata pelajaran : " . round($rata / 10, 2) . ")"; ?></td>
                                </tr>
                            </table>
                        </div> <!-- /.col-md-6 col-md-offset-3 -->
                    </div>
                    <!-- /.row -->
                    <h4 class="text-center">Dinyatakan :</h4>
                    <h2 class="text-center text-danger">- <?= strtoupper($is_pass); ?> -</h2>
                    <h4 class="text-center">
                        Dari Satuan Pendidikan <?= $sekolah[0]['nama_sekolah']; ?>&nbsp;<?= ucwords(strtolower($sekolah[0]['kabupaten'])); ?>
                        <br>
                        Tahun Pelajaran <?= date("Y", strtotime('-1 year')) . "/" .  date("Y"); ?>
                    </h4>

                    <div class="row">
                        <div class="col-xs-offset-6 col-xs-6">
                            <p class="text-center">Jakarta, <?= "29 Mei 2019";
                                                            ?></p>
                            <p class="text-center">Kepala <?= $sekolah[0]['nama_sekolah']; ?></p>
                            <p class="text-center">TTD</p>
                            <br>
                            <p class="text-center"><br>
                                <strong><?= $sekolah[0]['kepala_sekolah']; ?></strong>
                                <br>
                                NIP
                            </p>
                        </div>
                    </div>
                    <a href="<?= site_url('siswa/cetak/' . $users[0]['student_id']); ?>" class="btn btn-success btn-flat pull-right" target="_new"><i class="fa fa-print" aria-hidden="true"></i> Cetak</a>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->

</section><!-- /.content -->

<?php $this->load->view('template/js'); ?>

<!--tambahkan custom js disini-->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/pages/dashboard2.js') ?>" type="text/javascript"></script>

<?php $this->load->view('template/foot'); ?>