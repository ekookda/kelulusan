<?php
$this->load->helper('form');

echo doctype('html5');
echo "<html>";
echo "<head>";

// meta
echo meta('description', 'Sistem Informasi Pengumuman Kelulusan');
echo meta('Content-type', 'text/html; charset=utf-8', 'equiv');
echo meta('X-UA-Compatible', 'IE=edge');
echo meta('keywords', 'Kelulusan, siswa');
// <!-- Tell the browser to be responsive to screen width -->
echo meta('', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport');

echo "<title>:. Simpul .:</title>";

// link_tag
echo link_tag('assets/AdminLTE-2.0.5/pages/comingsoon/images/icons/favicon.ico', 'stylesheet', 'text/css');
echo link_tag('assets/AdminLTE-2.0.5/pages/comingsoon/vendor/bootstrap/css/bootstrap.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/AdminLTE-2.0.5/pages/comingsoon/fonts/font-awesome-4.7.0/css/font-awesome.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css', 'stylesheet', 'text/css');
echo link_tag('assets/parsley.css', 'stylesheet', 'text/css');
?>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLTE-2.0.5/pages/comingsoon/'); ?>fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLTE-2.0.5/pages/comingsoon/'); ?>vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLTE-2.0.5/pages/comingsoon/'); ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLTE-2.0.5/pages/comingsoon/css/util.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLTE-2.0.5/pages/comingsoon/css/main.css'); ?>">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    body {
        text-align: center;
        /* background-image: url("https://i0.wp.com/jaysonfeltner.com/wp-content/uploads/2012/05/bigstock-Graduation-Background-Vector-12770498.jpg"); */
        background-repeat: no-repeat;
        background-position: center center;
        font-family: sans-serif;
        font-weight: 100;
    }

    .wrapper {
        display: flex;
        align-items: center;
        width: 100%;
        height: 100%;
        background: #eee;
    }
</style>
<?php
echo "</head>";
echo "<body>";
?>

<div class="wrapper">
    <div class="container-fluid">
        <div class="alert alert-success text-center">
            <h1 id="getting-started"></h1>
        </div>
    </div>
</div>

<!-- javascript -->
<script src="<?= base_url() . 'assets/jquery/jquery.min.js'; ?>"></script>
<script src="<?= base_url() . 'assets/jquery-countdown/jquery.countdown.min.js'; ?>"></script>
<script type="text/javascript">
    <?php
    $jadwal = $this->db->get('tbl_jadwal')->result_array();
    $jadwal = date('Y/m/d H:i:s', strtotime($jadwal[0]['start_date']));
    ?>
    $("#getting-started").countdown("<?= $jadwal; ?>", {
            elapse: true
        })
        .on('update.countdown', function(event) {
            if (event.elapsed) {
                window.location.href = "<?= site_url('login'); ?>";
            } else {
                $(this).text(event.strftime('%D hari %H jam %M Menit %S Detik Lagi Menuju Waktu Pengumuman Kelulusan'));
            }
        });
</script>
</body>

</html>