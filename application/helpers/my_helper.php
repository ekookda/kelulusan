<?php

/**
 * Function check login
 */
if (!function_exists('is_logged_in')) {
    function is_logged_in()
    {
        $CI = &get_instance();
        $logged_in = $CI->session->userdata('logged_in');

        if (!isset($logged_in)) {
            $CI->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Anda belum login!</div>');
            redirect('login');
        } else {
            return true;
        }
    }
}

/**
 * Function Countdown Time
 */
if (!function_exists('countdown')) {
    function countdown()
    {
        $CI = &get_instance();
        $load_helper = $CI->load->helper('date');

        date_default_timezone_set('Asia/Jakarta');
        $waktu_sekarang = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y'));
        $waktu_finished = mktime(10, 00, 0, date('m'), date('d'), date('Y'));

        $selisih = $waktu_finished - $waktu_sekarang;
        return $selisih;
    }
}

/**
 * Function User ID
 */
if (!function_exists('uid')) {
    function uid()
    {
        $CI = &get_instance();
        $CI->load->model('C_admin', 'model');
        $uid = $CI->model->getWhere('tbl_users', ['id' => $CI->session->userdata('UID')])->result_array();

        return $uid;
    }
}

/**
 * Function Format Tanggal Indonesia
 */
if (!function_exists('tanggal_indo')) {
    function tanggal_indo($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[(int)$split[1]] . ' ' . $split[0];
    }
}
