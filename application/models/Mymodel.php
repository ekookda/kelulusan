<?php defined('BASEPATH') or exit('No direct script access allowed');

class Mymodel extends CI_Model
{
    public function getData($table, $where)
    {
        $get = $this->db->get_where($table, $where);
        return $get;
    }

    public function getWhere($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    public function updateData($where, $table, $data)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }
}
