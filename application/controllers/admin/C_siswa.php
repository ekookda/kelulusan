<?php defined('BASEPATH') or exit('No direct script access allowed');

class C_siswa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_admin', 'model');
        $this->load->helper(['my', 'download', 'debug']);
        is_logged_in();
        if ($this->session->userdata('role_id') != 1) redirect('login');
    }

    public function index()
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $data['data_siswa'] = $this->_data_siswa();
        $data['pengunjung'] = $this->_counter();
        $this->load->view('admin/siswa/v_index', $data);
    }

    public function download()
    {
        force_download('assets/example/contoh-data.xls', null);
    }

    private function _data_siswa()
    {
        $query = "SELECT `tbl_student`.id, `tbl_student`.name, `tbl_student`.*, `tbl_scores`.*
                    FROM `tbl_student`
              INNER JOIN `tbl_scores`
                      ON `tbl_student`.id = `tbl_scores`.student_id";

        if ($this->model->setQuery($query)->num_rows() > 0) {
            return $this->model->setQuery($query)->result_array();
        }
        return false;
    }

    public function add()
    {
        $this->form_validation->set_rules('nomor_peserta', 'Nomor Peserta', 'trim|required|min_length[13]');
        $this->form_validation->set_rules('nama_peserta', 'Nama Peserta', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'trim|required|numeric');
        $this->form_validation->set_rules('n_bing', 'Nilai Bahasa Inggris', 'trim|required|decimal', array('decimal' => 'Penulisan desimal gunakan tanda titik (.)'));
        $this->form_validation->set_rules('n_bindo', 'Nilai Bahasa Indonesia', 'trim|required|decimal', array('decimal' => 'Penulisan desimal gunakan tanda titik (.)'));
        $this->form_validation->set_rules('n_mat', 'Nilai Matematika', 'trim|required|decimal', array('decimal' => 'Penulisan desimal gunakan tanda titik (.)'));
        $this->form_validation->set_rules('n_peminatan', 'Nilai IPA', 'trim|required|decimal', array('decimal' => 'Penulisan desimal gunakan tanda titik (.)'));
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('is_pass', 'Status', 'required');

        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/siswa/v_add', $data);
        } else {
            $this->_save();
        }
    }

    private function _save()
    {
        $password = date("dmY", strtotime($this->input->post('tgl_lahir', true)));

        $tbl_student = array(
            'id'         => $this->input->post('nomor_peserta', true),
            'name'       => $this->input->post('nama_peserta', true),
            'gender'    => $this->input->post('gender', true),
            'birthday'   => $this->input->post('tgl_lahir', true),
            'telp'       => $this->input->post('no_telp', true),
            'created_at' => time(),
            'updated_at' => time()
        );

        $tbl_scores = array(
            'student_id'  => $this->input->post('nomor_peserta', true),
            'n_bindo'     => (float)$this->input->post('n_bindo', true),
            'n_bing'      => (float)$this->input->post('n_bing', true),
            'n_mat'       => (float)$this->input->post('n_mat', true),
            'n_peminatan' => (float)$this->input->post('n_peminatan', true),
            'n_total'     => (float)$this->input->post('n_bindo', true) + (float)$this->input->post('n_bing', true) + (float)$this->input->post('n_mat', true) + (float)$this->input->post('n_peminatan', true),
            'n_rata'      => ((float)$this->input->post('n_bindo', true) + (float)$this->input->post('n_bing', true) + (float)$this->input->post('n_mat', true) + (float)$this->input->post('n_peminatan', true)) / 4,
            'is_pass'     => $this->input->post('is_pass', true)
        );

        $tbl_users = array(
            'role_id'    => 2,
            'username'   => $this->input->post('nomor_peserta', true),
            'password'   => password_hash($password, PASSWORD_DEFAULT),
            'name'       => $this->input->post('nama_peserta', true),
            'is_active'  => 1,
            'updated_at' => time()
        );

        if ($this->model->save('tbl_student', $tbl_student) && $this->model->save('tbl_scores', $tbl_scores) && $this->model->save('tbl_users', $tbl_users)) {
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Congratulation! Your data has been created!</div>');
            redirect('admin/siswa');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Data not registered!</div>');
            redirect('admin/siswa/add');
        }
    }

    public function detail($student_id)
    {
        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();
        $query = "SELECT `tbl_student`.*,
                         `tbl_scores`.*,
                         `tbl_users`.role_id, `tbl_users`.username
                    FROM `tbl_student`
              INNER JOIN `tbl_scores`
                      ON `tbl_student`.id = `tbl_scores`.student_id
              INNER JOIN `tbl_users`
                      ON `tbl_student`.id = `tbl_users`.username
                   WHERE `tbl_student`.id = '$student_id'";
        $data['profile'] = $this->model->setQuery($query)->result_array();

        $this->load->view('admin/siswa/v_detail', $data);
    }

    public function edit($student_id)
    {
        $this->form_validation->set_rules('nama_peserta', 'Nama Peserta', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'trim|required|numeric');
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules(
            'n_bing',
            'Nilai Bahasa Inggris',
            'trim|required|decimal',
            array('decimal' => 'Penulisan desimal gunakan tanda titik (.)')
        );
        $this->form_validation->set_rules(
            'n_bindo',
            'Nilai Bahasa Indonesia',
            'trim|required|decimal',
            array('decimal' => 'Penulisan desimal gunakan tanda titik (.)')
        );
        $this->form_validation->set_rules(
            'n_mat',
            'Nilai Matematika',
            'trim|required|decimal',
            array('decimal' => 'Penulisan desimal gunakan tanda titik (.)')
        );
        $this->form_validation->set_rules(
            'n_peminatan',
            'Nilai IPA',
            'trim|required|decimal',
            array('decimal' => 'Penulisan desimal gunakan tanda titik (.)')
        );
        $this->form_validation->set_rules('is_pass', 'Status', 'required');

        $data['users'] = $this->model->getWhere('tbl_users', ['id' => $this->session->userdata('UID')])->result_array();

        if ($this->form_validation->run() == false) {
            $query = "SELECT `tbl_student`.*,
                             `tbl_scores`.*,
                             `tbl_users`.role_id, `tbl_users`.username
                        FROM `tbl_student`
                  INNER JOIN `tbl_scores`
                          ON `tbl_student`.id = `tbl_scores`.student_id
                  INNER JOIN `tbl_users`
                          ON `tbl_student`.id = `tbl_users`.username
                       WHERE `tbl_student`.id = '$student_id'";
            $data['profile'] = $this->model->setQuery($query)->result_array();
            $this->load->view('admin/siswa/v_edit', $data);
        } else {
            $this->_update();
        }
    }

    private function _update()
    {
        $student_id = $this->input->post('student_id', true);

        $tbl_student = array(
            'id'         => $student_id,
            'name'       => $this->input->post('nama_peserta', true),
            'gender'     => $this->input->post('gender', true),
            'telp'       => $this->input->post('no_telp', true),
            'birthday'   => $this->input->post('tgl_lahir', true),
            'updated_at' => time()
        );

        $tbl_scores = array(
            'n_bindo'     => (float)$this->input->post('n_bindo', true),
            'n_bing'      => (float)$this->input->post('n_bing', true),
            'n_mat'       => (float)$this->input->post('n_mat', true),
            'n_peminatan' => (float)$this->input->post('n_peminatan', true),
            'n_total'     => (float)$this->input->post('n_bindo', true) + (float)$this->input->post('n_bing', true) + (float)$this->input->post('n_mat', true) + (float)$this->input->post('n_peminatan', true),
            'n_rata'      => ((float)$this->input->post('n_bindo', true) + (float)$this->input->post('n_bing', true) + (float)$this->input->post('n_mat', true) + (float)$this->input->post('n_peminatan', true)) / 4,
            'is_pass'     => $this->input->post('is_pass', true),
        );

        $tbl_users = array(
            'name'       => $this->input->post('nama_peserta', true),
            'updated_at' => time()
        );

        if ($this->model->update('tbl_student', $tbl_student, ['id' => $student_id]) && $this->model->update('tbl_scores', $tbl_scores, ['student_id' => $student_id]) && $this->model->update('tbl_users', $tbl_users, ['username' => $student_id])) {
            $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Congratulation! Your data has been updated!</div>');
            redirect('admin/siswa');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Data not updated!</div>');
            $this->edit($student_id);
        }
    }

    public function delete($student_id)
    {
        $role_id = 2;
        $this->model->delete('tbl_student', 'id', $student_id);
        $this->model->setQuery("DELETE FROM `tbl_users` WHERE role_id='$role_id' AND username='$student_id'");
        $this->model->delete('tbl_scores', 'student_id', $student_id);

        $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Congratulation! Your data has been deleted!</div>');
        redirect('admin/siswa');
    }

    public function delete_all()
    {
        $role_id = 2;
        $qry = $this->model->setQuery("SELECT * FROM `tbl_users` WHERE role_id='$role_id'")->num_rows();
        for ($i = 0; $i < $qry; $i++) {
            $this->model->setQuery("DELETE FROM `tbl_users` WHERE role_id='$role_id'");
        }

        $this->model->delete_all('tbl_student');
        $this->model->delete_all('tbl_scores');

        $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Congratulation! Your data has been deleted!</div>');
        redirect('admin/siswa');
    }

    private function _counter()
    {
        $web_browser = $_SERVER['HTTP_USER_AGENT'];
        $chrome = '/Chrome/';
        $firefox = '/Firefox/';
        $ie = '/IE/';
        if (preg_match($chrome, $web_browser))
            $d = "Chrome/Opera";
        if (preg_match($firefox, $web_browser))
            $d = "Firefox";
        if (preg_match($ie, $web_browser))
            $d = "IE";

        // untuk mengambil informasi dari pengunjung
        $ipaddress = $_SERVER['REMOTE_ADDR'] . "";
        $web_browser = $d;
        $tanggal = date('Y-m-d');
        $kunjungan = 1;
        // Daftarkan Kedalam Session Lalu Simpan Ke Database
        if (!$this->session->set_userdata('counterdb')) {
            $this->session->set_userdata('counterdb', $ipaddress);
            $hit = [
                'tanggal' => $tanggal,
                'ip_address' => $ipaddress,
                'pengunjung' => $kunjungan,
                'browser' => $d
            ];
            $this->db->insert('tbl_counter', $hit);
        }

        // Hitung Jumlah Visitor
        $kemarin = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
        $data['hari_ini'] = $this->db->query('SELECT SUM(pengunjung) AS hari_ini FROM tbl_counter WHERE tanggal ="' .  date("Y-m-d") . '"')->result_array();
        $data['kemarin'] = $this->db->query('SELECT SUM(pengunjung) AS kemarin FROM tbl_counter WHERE tanggal="' . $kemarin . '"')->result_array();
        $data['total'] = $this->db->query('SELECT SUM(pengunjung) AS total FROM tbl_counter')->result_array();

        return $data;
    }
}
