<?php defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$open = date('d-m-Y H:i:s', mktime(10, 0, 0, 5, 29, 2019));
		$now = date('d-m-Y H:i:s');
		if ($now < $open) {
			$this->_countdown();
		} else {
			redirect('login');
		}
	}

}
