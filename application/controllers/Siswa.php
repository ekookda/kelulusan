<?php defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['my', 'debug']);
        $this->load->model('M_smp', 'siswa');
        is_logged_in();
    }

    public function index()
    {
        $data['sekolah'] = $this->siswa->getAll('tbl_school')->result_array();
        $data['users'] = $this->_student();
        $this->load->view('siswa/v_index', $data);
    }

    // Lihat hasil pengumuman siswa
    private function _student()
    {
        $username = $this->session->userdata('username');
        $query = "SELECT `tbl_student`.name, `tbl_student`.is_paid, `tbl_scores`.*
					FROM `tbl_student`
			  INNER JOIN `tbl_scores`
					  ON `tbl_student`.id = `tbl_scores`.student_id
				   WHERE `tbl_student`.id = '$username'";
        $num_rows = $this->siswa->setQuery($query)->num_rows();

        if ($num_rows > 0) {
            return $this->siswa->setQuery($query)->result_array();
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Data not found!</div>');
            // redirect('signin');
            return false;
        }
    }

    public function cetak($id)
    {
        $username = $this->session->userdata('username');
        $query = "SELECT `tbl_student`.name, `tbl_student`.is_paid, `tbl_scores`.*
					FROM `tbl_student`
			  INNER JOIN `tbl_scores`
					  ON `tbl_student`.id = `tbl_scores`.student_id
                   WHERE `tbl_student`.id = '$username'";
        $data['sekolah'] = $this->siswa->getAll('tbl_school')->result_array();
        $data['siswa'] = $this->siswa->setQuery($query)->result_array();
        $this->load->view('siswa/v_cetak', $data);
    }
}
