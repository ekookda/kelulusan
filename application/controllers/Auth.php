<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mymodel', 'model');
        $this->load->library('form_validation');
        $this->load->helper('security');
    }

    public function login_user()
    {
        // rules validation
        $rules = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|xss_clean'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|xss_clean|min_length[6]'
            )
        );
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) $this->load->view('login_form'); // gagal validasi
        else $this->_login();
    }

    private function _login()
    {
        // lolos validasi, ambil value input
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        $where = ['username' => $username];

        $user = $this->model->getData('tbl_users', $where)->row_array();

        if ($user) {
            if (password_verify($password, $user['password'])) {
                // User melaporkan ke orang tua
                if ($user['is_active'] != 1) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Akun anda belum aktif!</div>');
                } else {
                    $sess_data = array(
                        'UID'       => $user['id'],
                        'username'  => $user['username'],
                        'role_id'   => $user['role_id'],
                        'logged_in' => true
                    );
                    $this->session->set_userdata($sess_data);
                    $this->model->updateData($where, 'tbl_users', ['updated_at' => time()]);

                    // Redirect Page
                    if ($user['role_id'] == 1) redirect('admin');
                    else redirect('siswa');
                }
            } else {
                // password tidak cocok
                $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Password yang anda masukkan salah!</div>');
                $this->load->view('login_form');
            }
        } else {
            // username tidak cocok
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-center" role="alert">Username belum terdaftar!</div>');
            $this->load->view('login_form');
        }
    }

    public function logout()
    {
        $sess_data = ['logged_in', 'role_id', 'UID', 'username'];
        $this->session->unset_userdata($sess_data);
        $this->session->set_flashdata('message', '<div class="alert alert-success text-center" role="alert">Anda telah logout!</div>');
        redirect('welcome');
    }
}
