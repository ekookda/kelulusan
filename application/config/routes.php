<?php defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Login Pages
$route['login'] = 'Auth/login_user';
$route['logout'] = 'Auth/logout';
$route['auth'] = 'Auth/login_user';

// Pages Admin
$route['admin'] = 'admin/C_admin';
$route['admin/dashboard'] = 'admin/C_admin';

$route['admin/siswa'] = 'admin/C_siswa';
$route['admin/siswa/add'] = 'admin/C_siswa/add';
$route['admin/siswa/save'] = 'admin/C_siswa/set_siswa';
$route['admin/siswa/detail/(:any)'] = 'admin/C_siswa/detail/$1';
$route['admin/siswa/edit/(:any)'] = 'admin/C_siswa/edit/$1';
$route['admin/siswa/update'] = 'admin/C_siswa/edit';
$route['admin/siswa/delete/(:any)'] = 'admin/C_siswa/delete/$1';
$route['admin/siswa/truncate'] = 'admin/C_siswa/delete_all';
$route['admin/siswa/download'] = 'admin/C_siswa/download';
$route['admin/siswa/upload'] = 'Excel/import';

$route['admin/report'] = 'admin/C_laporan';
$route['admin/report/cetak'] = 'admin/C_laporan/cetak';
$route['admin/report/cetak/filter/(:num)'] = 'admin/C_laporan/cetak';
$route['admin/laporan'] = 'admin/C_laporan';
$route['admin/laporan/cetak'] = 'admin/C_laporan/cetak';
$route['admin/laporan/cetak/filter/(:num)'] = 'admin/C_laporan/cetak';

$route['admin/jadwal'] = 'admin/C_jadwal';
$route['admin/sekolah'] = 'admin/C_sekolah';

// Pages Siswwa
$route['signin'] = 'welcome/siswa';
$route['siswa'] = 'Siswa';
$route['siswa/(:any)'] = 'Siswa/$1';
$route['siswa/cetak/(:any)'] = 'Siswa/cetak/$1';
